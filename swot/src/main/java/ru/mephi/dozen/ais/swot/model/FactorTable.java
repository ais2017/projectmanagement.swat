package ru.mephi.dozen.ais.swot.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FactorTable {
    private Integer id;
    private String name;
    private List<Parameter> parameters = new ArrayList<>();

    public FactorTable(){

    }

    public FactorTable(Integer id, String name, List<Parameter> parameters) {
        this.id = id;
        this.name = name;
        this.parameters = parameters;
    }
}
