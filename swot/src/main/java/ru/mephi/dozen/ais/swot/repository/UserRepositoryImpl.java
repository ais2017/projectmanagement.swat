package ru.mephi.dozen.ais.swot.repository;

import com.mongodb.*;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UserRepositoryImpl implements UserRepository {

    private MongoClient mongoClient;
    private DB database;
    private DBCollection collection;

    public UserRepositoryImpl(Properties prop) {
        mongoClient = new MongoClient( prop.getProperty("host"), Integer.valueOf(prop.getProperty("port")) );
        database = mongoClient.getDB(prop.getProperty("dbname"));
        collection = database.getCollection(prop.getProperty("table"));
    }

    @Override
    public List<User> getUsers() {
        DBCursor cursor = collection.find();
        List<User> result = new ArrayList<>();
        while (cursor.hasNext()) {
            DBObject next = cursor.next();
            result.add(new User(UserType.valueOf(next.get("type").toString()),
                    (Long) next.get("access_token")));
        }
        return result;
    }

    @Override
    public void addUser(User user) {
        BasicDBObject document = new BasicDBObject();
        document.put("type", user.getUserType().name());
        document.put("access_token", user.getAccessToken());
        collection.insert(document);
    }

    @Override
    public void deleteUser(Long accessToken) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("access_token", accessToken);
        collection.remove(searchQuery);
    }

    @Override
    public void updateCredentials(Long accessToken, UserType newType) {

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$set", new BasicDBObject().append("type", newType.name()));

        BasicDBObject searchQuery = new BasicDBObject().append("access_token", accessToken);
        collection.update(searchQuery, newDocument);
    }

    public void clear() {
        collection.drop();
    }
}
