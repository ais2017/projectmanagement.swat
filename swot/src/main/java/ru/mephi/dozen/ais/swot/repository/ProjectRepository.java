package ru.mephi.dozen.ais.swot.repository;

import ru.mephi.dozen.ais.swot.model.Factor;
import ru.mephi.dozen.ais.swot.model.FactorTable;
import ru.mephi.dozen.ais.swot.model.Parameter;
import ru.mephi.dozen.ais.swot.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository {
    List<Project> getProjects();
    void deleteProject(Project project);
    void addProject(Project project);
    Optional<Project> getProject(Integer id);
    Optional<Factor> getFactor(Integer id);
    Optional<FactorTable> getFactorTable(Integer id);
    void addFactorTable(Integer factorId, FactorTable factorTable);
    void deleteFactorTable(Integer factorId, FactorTable factorTable);
    void addFactor(Integer projectId, Factor factor);
    void updateFactor(Factor factor);
    void deleteFactor(Integer projectId, Factor factor);
    void addParameter(Integer factorTableId, Parameter parameter);
    void deleteParameter(Integer factorTableId, Parameter parameter);
}
