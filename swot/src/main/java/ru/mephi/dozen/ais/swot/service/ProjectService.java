package ru.mephi.dozen.ais.swot.service;

import ru.mephi.dozen.ais.swot.BionicAuthenticator;
import ru.mephi.dozen.ais.swot.Importer;
import ru.mephi.dozen.ais.swot.enums.ParameterType;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.*;
import ru.mephi.dozen.ais.swot.repository.ProjectRepository;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectService {

    private ProjectRepository projectRepository;

    private Importer importer;

    private UserService userService;

    private List<Project> projects = new ArrayList<>();

    public ProjectService(UserService userService){
        this.userService = userService;
    }

    public ProjectService(ProjectRepository projectRepository, UserService userService) {
        this.userService = userService;
        this.projectRepository = projectRepository;
        projects = projectRepository.getProjects();
    }

    public ProjectService(ProjectRepository projectRepository, Importer importer, UserService userService) {
        this.userService = userService;
        this.projectRepository = projectRepository;
        this.importer = importer;
        projects = projectRepository.getProjects();
    }

    public ProjectService(Importer importer, UserService userService) {
        this.importer = importer;
        this.userService = userService;
    }


    public List<Project> getProjects() {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                return projectRepository.getProjects();
            }
            return projects;
        }
        else
            throw new SecurityException();
    }

    public void deleteProject(Project project) {
        if (userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.deleteProject(project);
            }
            getProjects().remove(project);
        } else
            throw new SecurityException();
    }

    public void addProject(Project project) {
        if ((userService.getCurrentUserType() == UserType.BOSS)) {
            if (projectRepository != null) {
                projectRepository.addProject(project);
            }
            getProjects().add(project);
        } else
            throw new SecurityException();
    }

    public void importProject(Integer id) {
        if ((userService.getCurrentUserType() == UserType.BOSS)) {
            Project project = importer.importProject(id);
            projects.add(project);
            if (projectRepository != null) {
                projectRepository.addProject(project);
            }
        }
        else
            throw new SecurityException();
    }

    private Optional<Project> findProject(Integer id) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {

            if (projectRepository != null) {
                return projectRepository.getProject(id);
            }

            return projects.stream()
                    .filter(it -> it.getId().equals(id))
                    .findFirst();
        }
        else
            throw new SecurityException();
    }

    public Optional<List<Factor>> getFactorsForProject(Integer id) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            return findProject(id)
                    .map(Project::getFactors);
        }
        else {
            throw new SecurityException();
        }
    }

    public List<IntSummaryStatistics> getStatisticsForProject(Integer id) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {

            Optional<Project> project = findProject(id);
            if (project.isPresent()) {
                List<Parameter> collect = project.get().getFactors().stream()
                        .map(Factor::getTables)
                        .flatMap(Collection::stream)
                        .map(FactorTable::getParameters)
                        .flatMap(Collection::stream).collect(Collectors.toList());
                return Arrays.asList(
                        collect.stream().filter(it -> it.getParameterType() == ParameterType.STRENGTH)
                                .mapToInt(Parameter::getWeight).summaryStatistics(),
                        collect.stream().filter(it -> it.getParameterType() == ParameterType.OPPORTUNITY)
                                .mapToInt(Parameter::getWeight).summaryStatistics(),
                        collect.stream().filter(it -> it.getParameterType() == ParameterType.THREAT)
                                .mapToInt(Parameter::getWeight).summaryStatistics(),
                        collect.stream().filter(it -> it.getParameterType() == ParameterType.WEAKNESS)
                                .mapToInt(Parameter::getWeight).summaryStatistics()
                );
            }
            return null;
        }
        else
            throw new SecurityException();
    }

    public Optional<Project> getProjectById(Integer id) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                return projectRepository.getProject(id);
            }
            return projects.stream()
                    .filter(project -> project.getId().equals(id))
                    .findFirst();
        }
        else
            throw new SecurityException();
    }

    public Optional<Factor> getFactorById(Integer id) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                return projectRepository.getFactor(id);
            }
            return projects.stream().map(Project::getFactors)
                    .flatMap(Collection::stream)
                    .filter(factor -> factor.getId().equals(id))
                    .findFirst();
        }
        else
            throw new SecurityException();
    }

    public Optional<FactorTable> getFactorTableById(Integer id) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                return projectRepository.getFactorTable(id);
            }
            return projects.stream().map(Project::getFactors)
                    .flatMap(Collection::stream)
                    .map(Factor::getTables)
                    .flatMap(Collection::stream)
                    .filter(factorTable -> factorTable.getId().equals(id))
                    .findFirst();
        }
        else
            throw new SecurityException();
    }

    public void addFactor(Integer projectId, Factor factor) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.addFactor(projectId, factor);
            }
            getProjectById(projectId).map(project -> project.getFactors().add(factor));
        }
        else
            throw new SecurityException();
    }

    public void deleteFactor(Integer projectId, Factor factor) {
        if (userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.deleteFactor(projectId, factor);
            }
            getProjectById(projectId).map(project -> project.getFactors().remove(factor));
        }
        else
            throw new SecurityException();
    }

    public void addFactorTable(Integer factorId, FactorTable factorTable) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.addFactorTable(factorId, factorTable);
            }
            getFactorById(factorId).map(factor -> factor.getTables().add(factorTable));
        }
        else
            throw new SecurityException();
    }

    public void deleteFactorTable(Integer factorId, FactorTable factorTable) {
        if (userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.deleteFactorTable(factorId, factorTable);
            }
            getFactorById(factorId).map(factor -> factor.getTables().remove(factorTable));
        }
        else
            throw new SecurityException();
    }

    public void addParameter(Integer factorTableId, Parameter parameter) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.addParameter(factorTableId, parameter);
            }
            getFactorTableById(factorTableId).map(factorTable -> factorTable.getParameters().add(parameter));
        }
        else
            throw new SecurityException();
    }

    public void deleteParameter(Integer factorTableId, Parameter parameter) {
        if (userService.getCurrentUserType() == UserType.BOSS) {
            if (projectRepository != null) {
                projectRepository.deleteParameter(factorTableId, parameter);
            }
            getFactorTableById(factorTableId).map(factorTable -> factorTable.getParameters().remove(parameter));
        }
        else
            throw new SecurityException();
    }

    public void combineTables(Integer factorId, String name) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            Optional<Factor> factor = getFactorById(factorId);
            factor.ifPresent(it -> it.compressFactorTables(name));
            if (projectRepository != null) {
                projectRepository.addFactorTable(factorId, factor.get().getTables().stream().findFirst().get());
            }
        }
        else
            throw new SecurityException();
    }

    public void combineParameters(Integer factorId, List<String> parameterNames, String parameterName) {
        if (userService.getCurrentUserType() == UserType.ANALYSIST || userService.getCurrentUserType() == UserType.BOSS) {
            Optional<Factor> factor = getFactorById(factorId);
            factor.ifPresent(it -> it.getParameterMappings().put(parameterName, parameterNames));
            factor.ifPresent(Factor::combineParameterWithMappings);
            if (projectRepository != null) {
                projectRepository.updateFactor(factor.get());
            }
        }
        else
            throw new SecurityException();
    }

}
