package ru.mephi.dozen.ais.swot.view;

import ru.mephi.dozen.ais.swot.BionicAuthenticator;
import ru.mephi.dozen.ais.swot.enums.ParameterType;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.*;
import ru.mephi.dozen.ais.swot.repository.ProjectRepository;
import ru.mephi.dozen.ais.swot.repository.ProjectRepositoryImpl;
import ru.mephi.dozen.ais.swot.service.ProjectService;
import ru.mephi.dozen.ais.swot.service.UserService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Main extends JFrame {
    private JButton compressButton;
    private JButton deleteButton;
    private JButton addButton;
    private JTextField toDelete;
    private JTextField toAdd;
    private JPanel panel;
    private JLabel sLable;
    private JLabel oLable;
    private JLabel tLable;
    private JLabel wLable;
    private JTextField toAddType;
    private JTextField toAddWeight;
    private JTextField compressFrom;
    private JTextField compressTo;

    private ProjectService ps;

    public Main() {

        Properties prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "database");

        ProjectRepositoryImpl projectRepository = new ProjectRepositoryImpl(prop);
        // projectRepository.clear();

        User godUser = new User(UserType.BOSS, 1L);
        UserService us = new UserService(() -> godUser);
        us.login();
        ps = new ProjectService(projectRepository, us);
        if (ps.getProjects().size() == 0) {
            Factor factor = new Factor();
            factor.setId(1);
            factor.setName("factor");
            factor.setTables(Arrays.asList(new FactorTable(1, "table", new ArrayList<>())));
            Project pr = new Project();
            pr.setFactors(Arrays.asList(factor));
            pr.setName("Name");
            pr.setId(1);
            ps.addProject(pr);
        }

        redrawLabels();

        deleteButton.addActionListener(a -> deleteParameterWithId(Integer.valueOf(toDelete.getText())));
        addButton.addActionListener(a -> addParameterWithId(toAdd.getText(), toAddType.getText(), toAddWeight.getText()));

        compressButton.addActionListener(a -> compress());
        setContentPane(panel);
        pack();
        setPreferredSize(new Dimension(800, 800));
        setVisible(true);
    }
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> {
            JFrame.setDefaultLookAndFeelDecorated(true);
            new Main();
        });
    }

    private void compress() {
        ps.combineParameters(1, Arrays.asList(compressFrom.getText()), compressTo.getText());
        redrawLabels();
    }

    private List<Parameter> getParams(ParameterType parameterType) {
        return ps.getProjectById(1).get().getFactors().get(0).getTables().get(0)
                .getParameters()
                .stream()
                .filter(it -> it.getParameterType() == parameterType)
                .collect(Collectors.toList());
    }

    private List<Parameter> getParams() {
        return ps.getProjectById(1).get().getFactors().get(0).getTables().get(0)
                .getParameters();
    }

    private void addParameterWithId(String name, String type, String weight) {
        ps.addParameter(1,
                new Parameter(new Random().nextInt(10000), name, ParameterType.valueOf(type), Integer.valueOf(weight)));
        redrawLabels();
    }

    private void deleteParameterWithId(Integer id) {
        //ps.deleteParameter(1);
        ps.deleteParameter(1, getParams().stream().filter(it -> it.getId().equals(id)).findFirst().get());
        ps.getProjectById(1).get().getFactors().get(0).getTables().get(0).getParameters().removeIf(it -> it.getId().equals(id));
        redrawLabels();
    }

    private void redrawLabels() {
        sLable.setText(formatOutput(ParameterType.STRENGTH));
        wLable.setText(formatOutput(ParameterType.WEAKNESS));
        oLable.setText(formatOutput(ParameterType.OPPORTUNITY));
        tLable.setText(formatOutput(ParameterType.THREAT));
    }

    private String formatOutput(ParameterType parameterType) {
        StringBuilder sb = new StringBuilder();
        getParams(parameterType).forEach(parameter ->
                sb.append("id: ")
                        .append(parameter.getId())
                        .append(",name: ")
                        .append(parameter.getName())
                        .append(",weight: ")
                        .append(parameter.getWeight())
                        .append("<br>"));
        return "<html>" + parameterType.name() + "<br>" + sb.toString() + "</html>";
    }

}
