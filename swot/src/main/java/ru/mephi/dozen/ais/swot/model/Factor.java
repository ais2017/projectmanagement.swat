package ru.mephi.dozen.ais.swot.model;

import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class Factor {
    private Integer id;
    private String name;
    private List<FactorTable> tables = new ArrayList<>();

    private Map<String, List<String>> parameterMappings = new HashMap<>();

    public Factor() {

    }

    public Factor(Integer id, String name, List<FactorTable> tables) {
        this.id = id;
        this.name = name;
        this.tables = tables;
    }

    public Boolean compressFactorTables(String newName) {
        if (tables != null && tables.size() > 1) {
            tables = Collections.singletonList(
                    new FactorTable(
                            tables.get(0).getId(),
                            newName,
                            tables.stream()
                                    .map(FactorTable::getParameters)
                                    .flatMap(Collection::stream)
                                    .collect(Collectors.toList())));
            return true;
        }
        return false;
    }

    public void combineParameterWithMappings() {
        parameterMappings.forEach((key, value) -> combineParameters(value, key));
    }

    private void combineParameters(List<String> params, String param) {
        tables.stream()
                .map(FactorTable::getParameters)
                .flatMap(Collection::stream)
                .filter(it -> params.contains(it.getName()))
                .forEach(it -> it.setName(param));
    }
}
