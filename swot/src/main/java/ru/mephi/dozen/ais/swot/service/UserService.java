package ru.mephi.dozen.ais.swot.service;

import ru.mephi.dozen.ais.swot.BionicAuthenticator;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.User;
import ru.mephi.dozen.ais.swot.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserService {

    private User currentUser = new User(UserType.UNKNOWN, -1L);

    private UserRepository repository;

    private BionicAuthenticator bionicAuthenticator;

    private List<User> users = new ArrayList<>();

    public UserService(BionicAuthenticator bionicAuthenticator){
        this.bionicAuthenticator = bionicAuthenticator;
    }

    public UserService(UserRepository repository, BionicAuthenticator bionicAuthenticator) {
        this.repository = repository;
        this.bionicAuthenticator = bionicAuthenticator;
        users = repository.getUsers();
    }

    public List<User> getUsers() {
        if (repository != null) {
            users = repository.getUsers();
        }
        return users;
    }

    public Optional<User> getUser(Long id) {
        return users.stream().filter(it -> it.getAccessToken().equals(id)).findAny();
    }

    public Boolean addUser(User user) {
        if (bionicAuthenticator.getUser().getUserType() == UserType.ADMIN) {
            users.add(user);
            if (repository != null)
                repository.addUser(user);
            return true;
        }
        return false;
    }

    public Boolean deleteUser(Long accessToken) {
        if (bionicAuthenticator.getUser().getUserType() == UserType.ADMIN) {
            users.removeIf(it -> it.getAccessToken().equals(accessToken));
            if (repository != null) {
                repository.deleteUser(accessToken);
            }
            return true;
        }
        return false;
    }

    public Boolean updateCredentials(Long accessToken, UserType newType) {
        if (bionicAuthenticator.getUser().getUserType() == UserType.ADMIN) {
            if (getUser(accessToken).get().getUserType() == UserType.ADMIN && users.stream().filter(it -> it.getUserType() == UserType.ADMIN).count() == 1)
                return false;
            users.stream()
                    .filter(it -> it.getAccessToken().equals(accessToken))
                    .findFirst()
                    .ifPresent(it -> it.setUserType(newType));
            if (repository != null) {
                repository.updateCredentials(accessToken, newType);
            }
            return true;
        }
        return false;
    }

    public void login() {
        User user = bionicAuthenticator.getUser();
        //if (users.contains(user)) {
            currentUser = user;
        //}
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public UserType getCurrentUserType() {
        if (currentUser != null)
            return currentUser.getUserType();
        else
            return UserType.UNKNOWN;
    }

    public void logout() {
        currentUser = null;
    }
}
