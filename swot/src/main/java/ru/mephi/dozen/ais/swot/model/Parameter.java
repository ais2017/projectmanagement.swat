package ru.mephi.dozen.ais.swot.model;


import lombok.Data;
import ru.mephi.dozen.ais.swot.enums.ParameterType;

@Data
public class Parameter {
    private Integer id;
    private String name;
    private ParameterType parameterType;
    private Integer weight;

    public Parameter() {

    }

    public Parameter(Integer id, String name, ParameterType parameterType, Integer weight) {
        this.id = id;
        this.name = name;
        this.parameterType = parameterType;
        this.weight = weight;
    }
}
