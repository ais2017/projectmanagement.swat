package ru.mephi.dozen.ais.swot.repository;

import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.User;

import java.util.List;

public interface UserRepository {
    List<User> getUsers();
    void addUser(User user);
    void deleteUser(Long accessToken);
    void updateCredentials(Long accessToken, UserType newType);
}
