package ru.mephi.dozen.ais.swot;

import ru.mephi.dozen.ais.swot.enums.ParameterType;
import ru.mephi.dozen.ais.swot.model.Factor;
import ru.mephi.dozen.ais.swot.model.FactorTable;
import ru.mephi.dozen.ais.swot.model.Parameter;
import ru.mephi.dozen.ais.swot.model.Project;
import ru.mephi.dozen.ais.swot.repository.ProjectRepositoryImpl;

import javax.swing.*;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import javax.swing.plaf.basic.BasicOptionPaneUI.ButtonActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class GUI extends JFrame {

    ProjectRepositoryImpl projectRepository;

    private Factor factor = new Factor();
    private FactorTable factorTable;
    private FactorTable oneMoreFactorTable;
    private static final int  PARAMS_COUNT = 4;
    private Parameter toDeleteParam = new Parameter(10, "Param " + 10, ParameterType.OPPORTUNITY, 10 + 20 * 20);

    private Project project;

    public GUI() {

        super("Тестовое окно");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        factor.setId(1);
        factor.setName("Some Factor");

        List<Parameter> params = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            params.add(new Parameter(i, "Param " + i, ParameterType.STRENGTH, 10 + 20 * i));
        }
        params.add(toDeleteParam);

        factorTable = new FactorTable();
        factorTable.setId(1);
        factorTable.setName("Table 1");
        factorTable.setParameters(params);

        List<Parameter> oneMoreParams = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            oneMoreParams.add(new Parameter(i + PARAMS_COUNT, "Param " + i , ParameterType.WEAKNESS, 20 + 10 * i));
        }

        oneMoreParams.add(new Parameter(20, "Param " + 20, ParameterType.THREAT, 10 + 20 * 10));


        oneMoreFactorTable = new FactorTable();
        oneMoreFactorTable.setId(2);
        oneMoreFactorTable.setName("Table 2");
        oneMoreFactorTable.setParameters(oneMoreParams);

        factor.setTables(new ArrayList<>(Arrays.asList(factorTable, oneMoreFactorTable)));

        project = new Project(11, "Project1", new ArrayList<>(Arrays.asList(factor)));

        Properties prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "database");

        projectRepository = new ProjectRepositoryImpl(prop);
        projectRepository.clear();

        List<Project> projects = Collections.singletonList(project);
        System.out.println(projects.get(0).getFactors().get(0).getTables().get(0));

        JLabel label1 = new JLabel(projects.get(0).getFactors().get(0).getTables().get(0)
                .getParameters().stream()
                .filter(it -> it.getParameterType() == ParameterType.STRENGTH)
                .map(it -> it.getName() +": " + it.getWeight())
                .collect(Collectors.toList())
                .toString());
        JLabel label2 = new JLabel(projects.get(0).getFactors().get(0).getTables().get(0).getParameters().stream()
                .filter(it -> it.getParameterType() == ParameterType.OPPORTUNITY)
                .map(it -> it.getName() +": " + it.getWeight())
                .collect(Collectors.toList())
                .toString());

        JLabel label3 = new JLabel(projects.get(0).getFactors().get(0).getTables().get(0).getParameters().stream()
                .filter(it -> it.getParameterType() == ParameterType.WEAKNESS)
                .map(it -> it.getName() +": " + it.getWeight())
                .collect(Collectors.toList())
                .toString());

        JLabel label4 = new JLabel(projects.get(0).getFactors().get(0).getTables().get(0).getParameters().stream()
                .filter(it -> it.getParameterType() == ParameterType.THREAT)
                .map(it -> it.getName() +": " + it.getWeight())
                .collect(Collectors.toList())
                .toString());


        Container container = this.getContentPane();
        container.setLayout(new GridLayout(6,1));
        container.add(label1);
        container.add(label2);
        container.add(label3);
        container.add(label4);

        JButton b1 = new JButton("Добавить");
        b1.addActionListener(actionEvent -> {
            projects.get(0).getFactors().get(0).getTables().get(0).getParameters().add(new Parameter(21345, "ADDED", ParameterType.OPPORTUNITY, 10 + 20 * 20));
            System.out.println(projects.get(0).getFactors().get(0).getTables().get(0).getParameters());
            label3.setText(new Parameter(21345, "ADDED", ParameterType.OPPORTUNITY, 10 + 20 * 20).toString());

        });
        JButton b2 = new JButton("Удалить");
        b2.addActionListener(actionEvent -> {
            projects.get(0).getFactors().get(0).getTables().get(0).getParameters().remove(new Parameter(0, "ADDED", ParameterType.OPPORTUNITY, 10 + 20 * 20));
            label3.setText("");
        });

        container.add(b1);
        container.add(b2);

        setPreferredSize(new Dimension(800, 800));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                new GUI();
            }
        });
    }

}
