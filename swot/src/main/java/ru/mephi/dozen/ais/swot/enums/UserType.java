package ru.mephi.dozen.ais.swot.enums;

public enum UserType {
    ADMIN, ANALYSIST, BOSS, UNKNOWN
}
