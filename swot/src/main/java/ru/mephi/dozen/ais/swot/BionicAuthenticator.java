package ru.mephi.dozen.ais.swot;

import ru.mephi.dozen.ais.swot.model.User;

public interface BionicAuthenticator {
    User getUser();
}
