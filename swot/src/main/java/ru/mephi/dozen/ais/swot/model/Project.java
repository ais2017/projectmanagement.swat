package ru.mephi.dozen.ais.swot.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Project {
    private Integer id;
    private String name;
    private List<Factor> factors = new ArrayList<>();

    public Project(){

    }

    public Project(Integer id, String name, List<Factor> factors) {
        this.id = id;
        this.name = name;
        this.factors = factors;
    }
}
