package ru.mephi.dozen.ais.swot.repository;

import com.mongodb.*;
import ru.mephi.dozen.ais.swot.enums.ParameterType;
import ru.mephi.dozen.ais.swot.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class ProjectRepositoryImpl implements ProjectRepository {

    private MongoClient mongoClient;
    private DB database;
    private DBCollection projects;
    private DBCollection factors;
    private DBCollection tables;
    private DBCollection params;


    public ProjectRepositoryImpl(Properties prop) {
        mongoClient = new MongoClient( prop.getProperty("host"), Integer.valueOf(prop.getProperty("port")) );
        database = mongoClient.getDB(prop.getProperty("dbname"));
        projects = database.getCollection("projects");
        factors = database.getCollection("factors");
        tables = database.getCollection("tables");
        params = database.getCollection("params");
    }

    @Override
    public List<Project> getProjects() {
        DBCursor cursor = projects.find();

        List<Project> result = new ArrayList<>();

        while (cursor.hasNext()) {
            DBObject next = cursor.next();
            Integer id = (Integer) next.get("project_id");
            result.add(getProject(id).get());
        }
        return result;
    }

    @Override
    public void deleteProject(Project project) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("project_id", project.getId());
        projects.remove(searchQuery);
    }

    @Override
    public void addProject(Project project) {
        BasicDBObject document = new BasicDBObject();
        document.put("project_id", project.getId());
        document.put("project_name", project.getName());
        document.put("factors", new ArrayList<>());

        projects.insert(document);
        for (Factor factor: project.getFactors()) {
            addFactor(project.getId(), factor);
        }
    }

    @Override
    public Optional<Project> getProject(Integer id) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("project_id", id);
        DBObject dbObject = projects.findOne(searchQuery);

        List<Integer> factorIndexes = (List<Integer>) dbObject.get("factors");

        List<Factor> fts = new ArrayList<>();

        for (Integer index:
                factorIndexes) {
            fts.add(getFactor(index).get());
        }

        Project pr = new Project((Integer) dbObject.get("project_id"), (String) dbObject.get("project_name"), fts);

        return Optional.of(pr);    }

    @Override
    public Optional<Factor> getFactor(Integer id) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("factor_id", id);
        DBObject dbObject = factors.findOne(searchQuery);

        List<Integer> tableIndexes = (List<Integer>) dbObject.get("tables");

        List<FactorTable> fts = new ArrayList<>();

        for (Integer index:
             tableIndexes) {
            fts.add(getFactorTable(index).get());
        }

        Factor f = new Factor((Integer) dbObject.get("factor_id"), (String) dbObject.get("factor_name"), fts);

        return Optional.of(f);
    }

    @Override
    public Optional<FactorTable> getFactorTable(Integer id) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("table_id", id);
        DBObject dbObject = tables.findOne(searchQuery);


        List<Integer> parameterIndexes = (List<Integer>) dbObject.get("parameters");
        List<Parameter> parameters = new ArrayList<>();

        for (Integer index:
                parameterIndexes) {
            BasicDBObject sq = new BasicDBObject();
            sq.put("parameter_id", index);
            DBObject one = params.findOne(sq);
            parameters.add(new Parameter(
                    (Integer) one.get("parameter_id"),
                    (String) one.get("parameter_name"),
                    ParameterType.valueOf((String) one.get("parameter_type")),
                    (Integer) one.get("parameter_weight")));
        }

        FactorTable ft = new FactorTable((Integer) dbObject.get("table_id"), (String) dbObject.get("table_name"), parameters);
        return Optional.of(ft);
    }

    @Override
    public void addFactorTable(Integer factorId, FactorTable factorTable) {
        BasicDBObject document = new BasicDBObject();
        document.put("table_id", factorTable.getId());
        document.put("table_name", factorTable.getName());
        document.put("parameters", new ArrayList<>());

        tables.insert(document);

        DBObject listItem = new BasicDBObject("tables", factorTable.getId());

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$push", listItem);

        BasicDBObject searchQuery = new BasicDBObject().append("factor_id", factorId);
        factors.update(searchQuery, newDocument);

        for (Parameter parameter: factorTable.getParameters()) {
            addParameter(factorTable.getId(), parameter);
        }
    }

    @Override
    public void deleteFactorTable(Integer factorId, FactorTable factorTable) {
        BasicDBObject sq = new BasicDBObject();
        sq.put("table_id", factorTable.getId());
        tables.remove(sq);

        DBObject listItem = new BasicDBObject("tables", factorTable.getId());

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$pull", listItem);

        BasicDBObject searchQuery = new BasicDBObject().append("factor_id", factorId);
        factors.update(searchQuery, newDocument);

        for (Parameter parameter: factorTable.getParameters()) {
            deleteParameter(factorTable.getId(), parameter);
        }
    }

    @Override
    public void addFactor(Integer projectId, Factor factor) {
        BasicDBObject document = new BasicDBObject();
        document.put("factor_id", factor.getId());
        document.put("factor_name", factor.getName());
        document.put("tables", new ArrayList<>());

        factors.insert(document);

        DBObject listItem = new BasicDBObject("factors", factor.getId());

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$push", listItem);

        BasicDBObject searchQuery = new BasicDBObject().append("project_id", projectId);
        projects.update(searchQuery, newDocument);


        for (FactorTable factorTable: factor.getTables()) {
            addFactorTable(factor.getId(), factorTable);
        }

    }

    @Override
    public void updateFactor(Factor factor) {
        BasicDBObject sq = new BasicDBObject();
        sq.put("factor_id", factor.getId());
        deleteFactor(0, factor);
        addFactor(0, factor);
    }

    @Override
    public void deleteFactor(Integer projectId, Factor factor) {
        BasicDBObject sq = new BasicDBObject();
        sq.put("factor_id", factor.getId());
        factors.remove(sq);

        DBObject listItem = new BasicDBObject("factors", factor.getId());

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$pull", listItem);

        BasicDBObject searchQuery = new BasicDBObject().append("project_id", projectId);
        projects.update(searchQuery, newDocument);

        for (FactorTable factorTable: factor.getTables()) {
            deleteFactorTable(factor.getId(), factorTable);
        }
    }

    @Override
    public void addParameter(Integer factorTableId, Parameter parameter) {
        BasicDBObject document = new BasicDBObject();
        document.put("parameter_id", parameter.getId());
        document.put("parameter_name", parameter.getName());
        document.put("parameter_type", parameter.getParameterType().toString());
        document.put("parameter_weight", parameter.getWeight());
        params.insert(document);

        DBObject listItem = new BasicDBObject("parameters", parameter.getId());

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$push", listItem);

        BasicDBObject searchQuery = new BasicDBObject().append("table_id", factorTableId);
        tables.update(searchQuery, newDocument);
    }

    @Override
    public void deleteParameter(Integer factorTableId, Parameter parameter) {
        BasicDBObject sq = new BasicDBObject();
        sq.put("parameter_id", parameter.getId());
        params.remove(sq);

        DBObject listItem = new BasicDBObject("parameters", parameter.getId());

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$pull", listItem);

        BasicDBObject searchQuery = new BasicDBObject().append("table_id", factorTableId);
        tables.update(searchQuery, newDocument);
    }

    public void clear() {
        projects.drop();
        factors.drop();
        tables.drop();
        params.drop();
    }
}
