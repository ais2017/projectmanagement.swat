package ru.mephi.dozen.ais.swot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.mephi.dozen.ais.swot.enums.UserType;

@Data
@AllArgsConstructor
public class User {
    private UserType userType;
    private Long accessToken;
}
