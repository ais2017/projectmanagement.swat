package ru.mephi.dozen.ais.swot.enums;

public enum ParameterType { STRENGTH, WEAKNESS, OPPORTUNITY, THREAT }
