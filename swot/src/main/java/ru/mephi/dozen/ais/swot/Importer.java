package ru.mephi.dozen.ais.swot;

import ru.mephi.dozen.ais.swot.model.Project;

import java.util.List;

public interface Importer {
    List<Project> getProjects();
    Project importProject(Integer id);

}
