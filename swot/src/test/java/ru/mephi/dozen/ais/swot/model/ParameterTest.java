package ru.mephi.dozen.ais.swot.model;

import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.enums.ParameterType;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ParameterTest {


    Parameter p;
    String name = "Name";
    Integer id = 1;
    Integer weight = 10;

    @Before
    public void setUp() throws Exception {
        p = new Parameter(id, name, ParameterType.STRENGTH, weight);
    }

    @Test
    public void getId() {
        assertEquals(p.getId(), (Integer)id);
    }

    @Test
    public void getName() {
        assertEquals(p.getName(), name);
    }

    @Test
    public void getParameterType() {
        assertEquals(p.getParameterType(), ParameterType.STRENGTH);
    }

    @Test
    public void getWeight() {
        assertEquals(p.getWeight(), weight);
    }

    @Test
    public void setId() {
        p.setId(2);
        assertEquals(p.getId(),(Integer) 2);
    }

    @Test
    public void setName() {
        p.setName("AA");
        assertEquals(p.getName(), "AA");
    }

    @Test
    public void setParameterType() {
        p.setParameterType(ParameterType.OPPORTUNITY);
        assertEquals(p.getParameterType(), ParameterType.OPPORTUNITY);
    }

    @Test
    public void setWeight() {
        p.setWeight(20);
        assertEquals(p.getWeight(), (Integer) 20);
    }
}