package ru.mephi.dozen.ais.swot.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.enums.ParameterType;
import ru.mephi.dozen.ais.swot.model.Factor;
import ru.mephi.dozen.ais.swot.model.FactorTable;
import ru.mephi.dozen.ais.swot.model.Parameter;
import ru.mephi.dozen.ais.swot.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

public class ProjectRepositoryImplTest {

    ProjectRepositoryImpl projectRepository;

    private Factor factor = new Factor();
    private FactorTable factorTable;
    private FactorTable oneMoreFactorTable;
    private static final int  PARAMS_COUNT = 4;
    private Parameter toDeleteParam = new Parameter(10, "Param " + 10, ParameterType.OPPORTUNITY, 10 + 20 * 20);

    private Project project;

    @Before
    public void setUp() throws Exception {

        Properties prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "database");

        projectRepository = new ProjectRepositoryImpl(prop);
        projectRepository.clear();

        factor.setId(1);
        factor.setName("Some Factor");

        List<Parameter> params = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            params.add(new Parameter(i, "Param " + i, ParameterType.STRENGTH, 10 + 20 * i));
        }
        params.add(toDeleteParam);

        factorTable = new FactorTable();
        factorTable.setId(1);
        factorTable.setName("Table 1");
        factorTable.setParameters(params);

        List<Parameter> oneMoreParams = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            oneMoreParams.add(new Parameter(i + PARAMS_COUNT, "Param " + i , ParameterType.WEAKNESS, 20 + 10 * i));
        }

        oneMoreParams.add(new Parameter(20, "Param " + 20, ParameterType.THREAT, 10 + 20 * 10));


        oneMoreFactorTable = new FactorTable();
        oneMoreFactorTable.setId(2);
        oneMoreFactorTable.setName("Table 2");
        oneMoreFactorTable.setParameters(oneMoreParams);

        factor.setTables(new ArrayList<>(Arrays.asList(factorTable, oneMoreFactorTable)));

        project = new Project(11, "Project1", new ArrayList<>(Arrays.asList(factor)));

        System.out.println(project);

        projectRepository.addProject(project);
    }

    @After
    public void tearDown() throws Exception {
        //projectRepository.clear();
    }

    @Test
    public void getProjects() {
        assertEquals(projectRepository.getProjects().size(),1);
    }

    @Test
    public void deleteProject() {
        projectRepository.deleteProject(project);
        assertEquals(projectRepository.getProjects().size(), 0);
    }

    @Test
    public void addProject() {
        projectRepository.addProject(new Project(13, "Project3", Arrays.asList(factor)));
        assertEquals(projectRepository.getProjects().size(),2);
    }

    @Test
    public void getProject() {
        assertEquals(projectRepository.getProject(11).get(), project);
    }

    @Test
    public void getFactor() {
        assertEquals(projectRepository.getFactor(1).get(), factor);
    }

    @Test
    public void addFactor() {
        projectRepository.addFactor(100, new Factor());
        assertEquals(projectRepository.getProject(11).get().getFactors().size(),1);
    }

    @Test
    public void updateFactor() {
        projectRepository.updateFactor(new Factor());
    }

    @Test
    public void deleteFactor() {
        projectRepository.deleteFactor(11, factor);
        assertTrue(projectRepository.getProject(11).get().getFactors().isEmpty());
    }

    @Test
    public void getFactorTable() {
        assertEquals(projectRepository.getFactorTable(1).get(), factorTable);
    }

    @Test
    public void addFactorTable() {
        projectRepository.addFactorTable(1 ,new FactorTable(132, "Name", Arrays.asList(new Parameter(123, "pp", ParameterType.OPPORTUNITY, 123))));
        assertEquals(projectRepository.getFactor(1).get().getTables().size(), 3);
    }

    @Test
    public void deleteFactorTable() {
        projectRepository.deleteFactorTable(1 ,factorTable);
        assertEquals(projectRepository.getFactor(1).get().getTables().size(), 1);
    }

    @Test
    public void addParameter() {
        projectRepository.addParameter(1 ,new Parameter(123, "pp", ParameterType.OPPORTUNITY, 123));
        assertEquals(projectRepository.getFactorTable(1).get().getParameters().size(), 6);
    }

    @Test
    public void deleteParameter() {
        projectRepository.deleteParameter(1, toDeleteParam);
        assertEquals(projectRepository.getFactorTable(1).get().getParameters().size(), 4);
    }
}