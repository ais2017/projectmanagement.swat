package ru.mephi.dozen.ais.swot.service;

import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.*;

import java.util.Arrays;

public class SecurityTest {


    private UserService userServiceUnknown;

    private ProjectService projectService;


    @Before
    public void setUp()  {

        userServiceUnknown = new UserService(() -> new User(UserType.UNKNOWN, 234L));
        userServiceUnknown.login();
        projectService = new ProjectService(userServiceUnknown);
    }

    @Test(expected = SecurityException.class)
    public void getProjects() {
        projectService.getProjects();
    }

    @Test(expected = SecurityException.class)
    public void deleteProject() {
        projectService.deleteProject(new Project(13, "Project3", null));
    }

    @Test(expected = SecurityException.class)
    public void addProject() {
        projectService.addProject(new Project(13, "Project3", null));
    }

    @Test(expected = SecurityException.class)
    public void importProject() {
        projectService.importProject(1);
    }

    @Test(expected = SecurityException.class)
    public void getFactorsForProject() {
        projectService.getFactorsForProject(11);
    }

    @Test(expected = SecurityException.class)
    public void getStatisticsForProject() {
        projectService.getStatisticsForProject(11);
    }

    @Test(expected = SecurityException.class)
    public void getProjectById() {
        projectService.getProjectById(12);
    }

    @Test(expected = SecurityException.class)
    public void getFactorById() {
        projectService.getFactorById(1);
    }

    @Test(expected = SecurityException.class)
    public void getFactorTableById() {
        projectService.getFactorTableById(1);
    }

    @Test(expected = SecurityException.class)
    public void addFactor() {
        projectService.addFactor(100, new Factor());
    }

    @Test(expected = SecurityException.class)
    public void deleteFactor() {
        projectService.deleteFactor(11, new Factor());
    }

    @Test(expected = SecurityException.class)
    public void addFactorTable() {
        projectService.addFactorTable(1 ,new FactorTable(132, "Name", null));
    }

    @Test(expected = SecurityException.class)
    public void deleteFactorTable() {
        projectService.deleteFactorTable(1 ,new FactorTable());
    }

    @Test(expected = SecurityException.class)
    public void addParameter() {
        projectService.addParameter(1 ,new Parameter());
    }

    @Test(expected = SecurityException.class)
    public void deleteParameter() {
        projectService.deleteParameter(1 ,new Parameter());
    }

    @Test(expected = SecurityException.class)
    public void combineTables() {
        projectService.combineTables(1, "newName");
    }

    @Test(expected = SecurityException.class)
    public void combineParameters() {
        projectService.combineParameters(1, Arrays.asList("Param 2", "Param 3"), "Param1");
        projectService.getFactorById(1).get().combineParameterWithMappings();
    }

}
