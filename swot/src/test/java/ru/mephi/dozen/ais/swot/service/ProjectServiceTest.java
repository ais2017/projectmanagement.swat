package ru.mephi.dozen.ais.swot.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.mephi.dozen.ais.swot.Importer;
import ru.mephi.dozen.ais.swot.enums.ParameterType;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.*;
import ru.mephi.dozen.ais.swot.repository.ProjectRepository;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ProjectServiceTest {

    private ProjectService projectService;

    private Factor factor = new Factor();
    private FactorTable factorTable;
    private FactorTable oneMoreFactorTable;
    private static final int  PARAMS_COUNT = 4;
    private Parameter toDeleteParam = new Parameter(10, "Param " + 10, ParameterType.OPPORTUNITY, 10 + 20 * 20);

    Project project2 = new Project(12, "Project2", null);

    @Before
    public void setUp() {

        factor.setId(1);
        factor.setName("Some Factor");

        List<Parameter> params = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            params.add(new Parameter(i, "Param " + i, ParameterType.STRENGTH, 10 + 20 * i));
        }
        params.add(toDeleteParam);

        factorTable = new FactorTable();
        factorTable.setId(1);
        factorTable.setName("Table 1");
        factorTable.setParameters(params);

        List<Parameter> oneMoreParams = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            oneMoreParams.add(new Parameter(i + PARAMS_COUNT, "Param " + i , ParameterType.WEAKNESS, 20 + 10 * i));
        }

        oneMoreParams.add(new Parameter(20, "Param " + 20, ParameterType.THREAT, 10 + 20 * 10));


        oneMoreFactorTable = new FactorTable();
        oneMoreFactorTable.setId(2);
        oneMoreFactorTable.setName("Table 2");
        oneMoreFactorTable.setParameters(oneMoreParams);

        factor.setTables(new ArrayList<>(Arrays.asList(factorTable, oneMoreFactorTable)));

        Project project = new Project(11, "Project1", new ArrayList<>(Arrays.asList(factor)));



        Importer importer = new Importer() {
            @Override
            public List<Project> getProjects() {
                return new ArrayList<>(Arrays.asList(new Project(), new Project()));
            }

            @Override
            public Project importProject(Integer id) {
                return new Project(id, "Imported", null);
            }
        };

        UserService userService = Mockito.mock(UserService.class);
        when(userService.getCurrentUserType()).thenReturn(UserType.BOSS);
        projectService = new ProjectService(importer, userService);

        projectService.getProjects().addAll(new ArrayList<>(Arrays.asList(project, project2)));
    }

    @Test
    public void projectsServiceConstructorTest() {
        ProjectService projectService = new ProjectService(new ProjectRepository() {
            @Override
            public List<Project> getProjects() {
                return Arrays.asList(new Project());
            }

            @Override
            public void deleteProject(Project project) {
            }

            @Override
            public void addProject(Project project) {
            }

            @Override
            public Optional<Project> getProject(Integer id) {
                return Optional.empty();
            }

            @Override
            public Optional<Factor> getFactor(Integer id) {
                return Optional.empty();
            }

            @Override
            public Optional<FactorTable> getFactorTable(Integer id) {
                return Optional.empty();
            }

            @Override
            public void addFactorTable(Integer factorId, FactorTable factorTable) {
            }

            @Override
            public void deleteFactorTable(Integer factorId, FactorTable factorTable) {
            }

            @Override
            public void addFactor(Integer projectId, Factor factor) {
            }

            @Override
            public void updateFactor(Factor factor) {
            }

            @Override
            public void deleteFactor(Integer projectId, Factor factor) {
            }

            @Override
            public void addParameter(Integer factorTableId, Parameter parameter) {
            }

            @Override
            public void deleteParameter(Integer factorTableId, Parameter parameter) {
            }
        }, new UserService(() -> null));
    }

    @Test
    public void constructorTest() {
        ProjectService projectService = new ProjectService(new ProjectRepository() {
            @Override
            public List<Project> getProjects() {
                return Arrays.asList(new Project());
            }

            @Override
            public void deleteProject(Project project) {
            }

            @Override
            public void addProject(Project project) {
            }

            @Override
            public Optional<Project> getProject(Integer id) {
                return Optional.empty();
            }

            @Override
            public Optional<Factor> getFactor(Integer id) {
                return Optional.empty();
            }

            @Override
            public Optional<FactorTable> getFactorTable(Integer id) {
                return Optional.empty();
            }

            @Override
            public void addFactorTable(Integer factorId, FactorTable factorTable) {
            }

            @Override
            public void deleteFactorTable(Integer factorId, FactorTable factorTable) {
            }

            @Override
            public void addFactor(Integer projectId, Factor factor) {
            }

            @Override
            public void updateFactor(Factor factor) {
            }

            @Override
            public void deleteFactor(Integer projectId, Factor factor) {
            }

            @Override
            public void addParameter(Integer factorTableId, Parameter parameter) {
            }

            @Override
            public void deleteParameter(Integer factorTableId, Parameter parameter) {
            }
        }, new Importer() {
            @Override
            public List<Project> getProjects() { return null; }
            @Override
            public Project importProject(Integer id) { return null; }
        }, new UserService(() -> null));
    }

    @Test
    public void getProjects() {
        assertEquals(projectService.getProjects().size(),2);
    }

    @Test
    public void deleteProject() {
        projectService.deleteProject(project2);
        assertEquals(projectService.getProjects().size(),1);
    }

    @Test
    public void addProject() {
        projectService.addProject(new Project(13, "Project3", null));
        assertEquals(projectService.getProjects().size(),3);
    }

    @Test
    public void importProject() {
        projectService.importProject(1);
        assertEquals(projectService.getProjects().size(),3);
    }

    @Test
    public void getFactorsForProject() {
        assertEquals(projectService.getFactorsForProject(11).get().size(),1);
    }

    @Test
    public void getStatisticsForProject() {
        assertEquals(projectService.getStatisticsForProject(11).size(), 4);
    }

    @Test
    public void getProjectById() {
        assertEquals(projectService.getProjectById(12).get(), project2);
    }

    @Test
    public void getFactorById() {
        assertEquals(projectService.getFactorById(1).get(), factor);
    }

    @Test
    public void getFactorTableById() {
        assertEquals(projectService.getFactorTableById(1).get(), factorTable);
    }

    @Test
    public void addFactor() {
        projectService.addFactor(100, new Factor());
        assertEquals(projectService.getProjectById(11).get().getFactors().size(),1);
    }

    @Test
    public void deleteFactor() {
        projectService.deleteFactor(11, factor);
        assertTrue(projectService.getProjectById(11).get().getFactors().isEmpty());
    }

    @Test
    public void addFactorTable() {
        projectService.addFactorTable(1 ,new FactorTable(132, "Name", null));
        //assertEquals(projectService.getFactorById(1).get().getTables().size(), 3);
    }

    @Test
    public void deleteFactorTable() {
        projectService.deleteFactorTable(1 ,factorTable);
    }

    @Test
    public void addParameter() {
        projectService.addParameter(1 ,new Parameter());
        assertEquals(projectService.getFactorTableById(1).get().getParameters().size(), 6);
    }

    @Test
    public void deleteParameter() {
        projectService.deleteParameter(1 ,toDeleteParam);
        assertEquals(projectService.getFactorTableById(1).get().getParameters().size(), 4);
    }

    @Test
    public void combineTables() {
        projectService.combineTables(1, "newName");
        assertEquals(projectService.getFactorById(1).get().getTables().get(0).getName(), "newName");
    }

    @Test
    public void combineParameters() {
        projectService.combineParameters(1, Arrays.asList("Param 2", "Param 3"), "Param1");
        assertEquals(projectService.getFactorById(1).get().getParameterMappings().size(), 1);
        projectService.getFactorById(1).get().combineParameterWithMappings();
        assertNotNull(projectService.getFactorById(1).get());
    }
}