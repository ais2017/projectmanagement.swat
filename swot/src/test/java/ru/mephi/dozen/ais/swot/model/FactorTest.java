package ru.mephi.dozen.ais.swot.model;

import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.enums.ParameterType;

import java.util.*;

import static org.junit.Assert.*;

public class FactorTest {

    private Factor factor = new Factor();
    private FactorTable factorTable;
    private FactorTable oneMoreFactorTable;
    private static final int  PARAMS_COUNT = 4;

    @Before
    public void setUp() throws Exception {

        factor.setId(1);
        factor.setName("Some Factor");

        List<Parameter> params = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            params.add(new Parameter(i, "Param " + i, ParameterType.STRENGTH, 10 + 20 * i));
        }

        factorTable = new FactorTable();
        factorTable.setId(1);
        factorTable.setName("Table 1");
        factorTable.setParameters(params);

        List<Parameter> oneMoreParams = new ArrayList<>();

        for (int i = 0; i < PARAMS_COUNT; i++) {
            oneMoreParams.add(new Parameter(i + PARAMS_COUNT, "Param " + i , ParameterType.WEAKNESS, 20 + 10 * i));
        }

        oneMoreFactorTable = new FactorTable();
        oneMoreFactorTable.setId(2);
        oneMoreFactorTable.setName("Table 2");
        oneMoreFactorTable.setParameters(oneMoreParams);

        factor.setTables(Arrays.asList(factorTable, oneMoreFactorTable));

        Map mappings = new HashMap();
        mappings.put("Param 1", Arrays.asList("Param 2", "Param 3"));
        mappings.put("Param 5", Arrays.asList("Param 0", "Param 3"));
        factor.setParameterMappings(mappings);
    }

    @Test
    public void noArgsConstructorTest(){
        Factor f = new Factor();
        assertNotNull(f);
    }

    @Test
    public void allArgsConstructorTest() {
        Factor f = new Factor(1, "SuperName", Arrays.asList(factorTable, oneMoreFactorTable));
        assertEquals(f.getId(), (Integer) 1);
        assertEquals(f.getName(), "SuperName");
        assertEquals(f.getTables().size(),2);
    }

    @Test
    public void getId() {
        assertEquals(factor.getId(), (Integer) 1);
    }

    @Test
    public void getName() {
        assertEquals(factor.getName(),"Some Factor");
    }

    @Test
    public void getFactorTables() {
        assertEquals(factor.getTables(), Arrays.asList(factorTable, oneMoreFactorTable));
    }

    @Test
    public void setId() {
        factor.setId(2);
        assertEquals(factor.getId(),(Integer) 2);
    }

    @Test
    public void setName() {
        String name = "Name";
        factor.setName(name);
        assertEquals(factor.getName(), name);
    }

    @Test
    public void setFactorTables() {
        factor.setTables(Arrays.asList(factorTable));
        assertEquals(factor.getTables().size(),1);
    }

    @Test
    public void compressFactorTables() {
        assertTrue(factor.compressFactorTables("Compressed Table"));
        assertEquals(factor.getTables().size(), 1);
        assertEquals(factor.getTables().get(0).getParameters().size(), PARAMS_COUNT * 2);

        assertFalse(factor.compressFactorTables("Compressed Table"));

        factor.setTables(null);
        assertFalse(factor.compressFactorTables(""));
    }

    @Test
    public void combineParameterWithMappings() {
        factor.combineParameterWithMappings();
        assertEquals(factor.getTables().stream()
                .map(FactorTable::getParameters)
                .flatMap(Collection::stream)
                .filter(it -> it.getName().equals("Param 2"))
                .count(), 0);
        assertEquals(factor.getTables().stream()
                .map(FactorTable::getParameters)
                .flatMap(Collection::stream)
                .filter(it -> it.getName().equals("Param 1"))
                .count(),6);
        assertEquals(factor.getTables().stream()
                .map(FactorTable::getParameters)
                .flatMap(Collection::stream)
                .filter(it -> it.getName().equals("Param 4"))
                .count(),0);
       assertEquals(factor.getTables().stream()
                .map(FactorTable::getParameters)
                .flatMap(Collection::stream)
                .filter(it -> it.getName().equals("Param 5"))
                .count(),2);
    }
}