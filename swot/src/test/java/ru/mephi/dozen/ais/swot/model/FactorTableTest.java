package ru.mephi.dozen.ais.swot.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.Assert.*;

public class FactorTableTest {

    FactorTable ft;
    String name = "Name";

    @Before
    public void setUp() throws Exception {
        ft = new FactorTable(1, name, Arrays.asList(new Parameter()));
    }

    @Test
    public void noArgsConstructor() {
        FactorTable ft = new FactorTable();
        assertNotNull(ft);
    }

    @Test
    public void allArgsConstructor() {
        FactorTable ft = new FactorTable(1, name, Arrays.asList(new Parameter()));
        assertEquals(ft.getId(), (Integer)1);
        assertEquals(ft.getName(), name);
        assertEquals(ft.getParameters().size(), 1);
    }

    @Test
    public void getId() {
        assertEquals(ft.getId(), (Integer) 1);
    }

    @Test
    public void getName() {
        assertEquals(ft.getName(), name);
    }

    @Test
    public void getParameters() {
        assertEquals(ft.getParameters().size(),1);
    }

    @Test
    public void setId() {
        ft.setId(2);
        assertEquals(ft.getId(), (Integer)2);
    }

    @Test
    public void setName() {
        ft.setName("newName");
        assertEquals(ft.getName(), "newName");
    }

    @Test
    public void setParameters() {
        ft.setParameters(Arrays.asList(new Parameter(), new Parameter()));
        assertEquals(ft.getParameters().size(), 2);
    }
}