package ru.mephi.dozen.ais.swot.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.User;

import java.util.Properties;

import static org.junit.Assert.*;

/* Proofs:
*   mongo database
*   show collections
*   db.users.find({})
* */
public class UserRepositoryImplTest {

    UserRepositoryImpl userRepository;

    private User toDelete = new User(UserType.BOSS, 123L);
    private User toUpdate = new User(UserType.UNKNOWN, 12L);

    @Before
    public void setUp() throws Exception {

        Properties prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "database");
        prop.setProperty("table", "users");

        userRepository = new UserRepositoryImpl(prop);
        userRepository.clear();
        //TODO: open connection, create DB, create collection
        userRepository.addUser(toDelete);
        userRepository.addUser(toUpdate);
    }

    @After
    public void turnDown() {
        // userRepository.clear();
    }

    @Test
    public void getUsers() {
        assertEquals(2, userRepository.getUsers().size());
    }

    @Test
    public void addUser() {
        userRepository.addUser(new User(UserType.ANALYSIST, 1234L));
        assertEquals(3, userRepository.getUsers().size());
    }

    @Test
    public void deleteUser() {
        userRepository.deleteUser(123L);
        assertEquals(1, userRepository.getUsers().size());
    }

    @Test
    public void updateCredentials() {
        userRepository.updateCredentials(12L, UserType.ANALYSIST);
        assertEquals(UserType.ANALYSIST, userRepository.getUsers().stream().filter(it -> it.getAccessToken() == 12L).findFirst().get().getUserType());
    }
}