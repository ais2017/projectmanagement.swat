package ru.mephi.dozen.ais.swot.model;

import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.enums.ParameterType;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ProjectTest {

    Project p;
    Integer id = 1;
    String name = "name";
    @Before
    public void setUp() throws Exception {
        p = new Project(id, name, Arrays.asList(new Factor()));
    }

    @Test
    public void noArgsConstructor() {
        Project pr = new Project();
        assertNotNull(pr);
    }

    @Test
    public void getId() {
        assertEquals(p.getId(), (Integer) 1);
    }

    @Test
    public void getName() {
        assertEquals(p.getName(), name);
    }

    @Test
    public void getFactors() {
        assertEquals(p.getFactors().size(),1);
    }

    @Test
    public void setId() {
        p.setId(2);
        assertEquals(p.getId(),(Integer) 2);
    }

    @Test
    public void setName() {
        p.setName("newName");
        assertEquals(p.getName(), "newName");
    }

    @Test
    public void setFactors() {
        p.setFactors(Arrays.asList(new Factor(),new Factor()));
        assertEquals(p.getFactors().size(),2);
    }
}