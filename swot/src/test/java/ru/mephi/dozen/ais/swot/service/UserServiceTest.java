package ru.mephi.dozen.ais.swot.service;

import org.junit.Before;
import org.junit.Test;
import ru.mephi.dozen.ais.swot.BionicAuthenticator;
import ru.mephi.dozen.ais.swot.enums.UserType;
import ru.mephi.dozen.ais.swot.model.User;
import ru.mephi.dozen.ais.swot.repository.UserRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest {

    private UserService userService;

    private UserService userServiceNoAdmin;

    @Before
    public void setUp()  {
        userService = new UserService(() -> new User(UserType.ADMIN, 1234L));
        userService.addUser(new User(UserType.ADMIN, 1234L));
        userService.addUser(new User(UserType.BOSS, 123L));
        userService.login();

        userServiceNoAdmin = new UserService(() -> new User(UserType.ANALYSIST, 1234L));
    }

    @Test
    public void userServiceConstructor() {
        userService = new UserService(new UserRepository() {
            @Override
            public List<User> getUsers() { return Arrays.asList(new User(UserType.ADMIN, 1234L)); }
            @Override
            public void addUser(User user) { }
            @Override
            public void deleteUser(Long accessToken) { }
            @Override
            public void updateCredentials(Long accessToken, UserType newType) { }
        }, () -> null);
        assertEquals(userService.getUsers().size(), 1);
    }

    @Test
    public void getCurrentUserTest() {
        assertEquals(userService.getCurrentUser(),new User(UserType.ADMIN, 1234L));
    }

    @Test
    public void getUsers() {
        assertEquals(userService.getUsers().size(),2);
    }

    @Test
    public void addUser() {
        assertTrue(userService.addUser(new User(UserType.ANALYSIST, 12321L)));
        assertEquals(userService.getUsers().size(),3);
    }

    @Test
    public void deleteUser() {
        assertTrue(userService.deleteUser(123L));
        assertEquals(userService.getUsers().size(), 1);
    }

    @Test
    public void updateCredentials() {
        assertTrue(userService.updateCredentials(123L, UserType.ANALYSIST));
        assertEquals(userService.getUsers().get(1).getUserType(), UserType.ANALYSIST);
    }

    @Test
    public void updateCredentialsLastAdmin() {
        assertFalse(userService.updateCredentials(1234L, UserType.ANALYSIST));
    }

    @Test
    public void loginTest() {
        userService.login();
        assertEquals(userService.getCurrentUserType(), UserType.ADMIN);
    }

    @Test
    public void logoutTest() {
        userService.logout();
        assertEquals(userService.getCurrentUserType(), UserType.UNKNOWN);
    }

    @Test
    public void declineAddUser() {
        assertFalse(userServiceNoAdmin.addUser(new User(UserType.ANALYSIST, 12321L)));
    }

    @Test
    public void declineDeleteUser() {
        assertFalse(userServiceNoAdmin.deleteUser(123L));
    }

    @Test
    public void declineUpdateCredentials() {
        assertFalse(userServiceNoAdmin.updateCredentials(123L, UserType.ANALYSIST));
    }
}